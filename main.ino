/*
 * ### Pitching machine firmware ###
 */

#include "DualVNH5019MotorShield.h"
#include "encoder.h"
#include "pid.h"

// pid
PID *pid1;
PID *pid2;

float wheelSetPoint1 = 380;
float wheelSetPoint2 = 380;

// Motor driver
#define MD_MAX_POWER 200  // Max 400
#define MD_MIN_POWER -200 // Min -400

DualVNH5019MotorShield md;

// Macro
#define run_every(t) for (static typeof(t) _lastTime; (typeof(t))((typeof(t))millis() - _lastTime) > (t); _lastTime += (t))

// Encoder
Encoder *encoder1;
Encoder *encoder2;

// Serial communication
int sCmds[3];
int cmdsSize = 0;

void setup() {
  // Initialize Serial
  Serial.begin(115200);

  // Initialize motor driver
  md.init();

  // Initialize pid controller PID(kp, ki, kd)
  pid1 = new PID(4, 0.001, 2);
  pid2 = new PID(4, 0.001, 2);

  // Initialize encoders
  encoder1 = new Encoder(0);
  encoder2 = new Encoder(1);

  // TIMER 2 for interrupt frequency 1000 Hz:
  cli();      // stop interrupts
  TCCR2A = 0; // set entire TCCR2A register to 0
  TCCR2B = 0; // same for TCCR2B
  TCNT2  = 0; // initialize counter value to 0
  // set compare match register for 1000 Hz increments
  OCR2A = 249; // = 16000000 / (64 * 1000) - 1 (must be <256)
  // turn on CTC mode
  TCCR2B |= (1 << WGM21);
  // Set CS22, CS21 and CS20 bits for 64 prescaler
  TCCR2B |= (1 << CS22) | (0 << CS21) | (0 << CS20);
  // enable timer compare interrupt
  TIMSK2 |= (1 << OCIE2A);
  sei(); // allow interrupts
}


void loop() {
  /*
   * Receive serial commands
   */
  cmdsSize = 0;

  while (Serial.available() > 0) {
    sCmds[cmdsSize++] = Serial.parseInt();
  }

  // Example of a valid data "15 22 37", the last number is the sum of the first two
  // Assure command integrity -> Check size, Check sum
  if (cmdsSize == 3 && ((sCmds[0] + sCmds[1]) == sCmds[2])) {
    // Define new setPoints
    wheelSetPoint1 = sCmds[0];
    wheelSetPoint2 = sCmds[1];
  }


  /*
   * Send wheels rpm through serial 2 (Hz)
   */
  run_every(500) { // (ms)
    float rpm1 = encoder1->get_rpm();
    float rpm2 = encoder2->get_rpm();
    Serial.println(String(rpm1) + " " + String(wheelSetPoint1) + " " + String(rpm2) + " " + String(wheelSetPoint2) + " " + String(rpm1 + wheelSetPoint1 + rpm2 + wheelSetPoint2));
  }
}


/*
 * Timer 2 ISR 1 (kHz)
 */
ISR(TIMER2_COMPA_vect) {
  // Get rpm, Call pid controller, Set motor driver
  md.setM1Speed(constrain(pid1->control(encoder1->get_rpm(), wheelSetPoint1), MD_MIN_POWER, MD_MAX_POWER));
  md.setM2Speed(constrain(pid2->control(encoder2->get_rpm(), wheelSetPoint2), MD_MIN_POWER, MD_MAX_POWER));
}
