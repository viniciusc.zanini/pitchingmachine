#include "encoder.h"


// constructor
Encoder::Encoder(const byte whichISR) : whichISR_(whichISR) {
  stableTime   = 1100; // (µs) to filter the encoder wave's noise
  previousTime = 0;    // (µs)
  currentTime  = 0;    // (µs)

  arcWidth = 0.050; // (m) arc width between the encoder steps
  radius   = 0.015; // (m) distance between the axis and the point of read

  spinTime  = 0;
  velocity  = 0;
  deltaTime = 0;

  currentRpm = 0;

  switch (whichISR_) {
  case 0:
    attachInterrupt(0, isr0, FALLING);
    instance0_ = this;
    break;

  case 1:
    attachInterrupt(1, isr1, FALLING);
    instance1_ = this;
    break;
  }
}

// ISR glue routines
void Encoder::isr0() {
  instance0_->handleInterrupt();
}

void Encoder::isr1() {
  instance1_->handleInterrupt();
}

// for use by ISR glue routines
Encoder *Encoder::instance0_;
Encoder *Encoder::instance1_;

// class instance to handle an interrupt
void Encoder::handleInterrupt() {
  currentTime = micros();
  deltaTime   = currentTime - previousTime;
  if (deltaTime >= stableTime) {
    velocity   = arcWidth / deltaTime;         // rotation velocity
    spinTime   = (TWO_PI * radius) / velocity; // spinTime = (2 * pi * radius) / velocity.
    currentRpm = 60000000 / spinTime;          // rpm = (a minute / spinTime)
  }
  previousTime = currentTime;
}

float Encoder::get_rpm() {
  return currentRpm;
}
