#ifndef ENCODER_h
#define ENCODER_h

#include <Arduino.h>

class Encoder {
private:
  static void isr0();
  static void isr1();

  const byte whichISR_;
  static Encoder *instance0_;
  static Encoder *instance1_;

  void handleInterrupt();

  unsigned int stableTime;             // (µs) to filter the encoder wave's noise
  volatile unsigned long previousTime; // (µs)
  volatile unsigned long currentTime;  // (µs)

  float arcWidth; // (m) arc width between the encoder steps
  float radius;   // (m) distance between the axis and the point of read

  float spinTime;
  float velocity;
  float deltaTime;

  float currentRpm;

public:
  Encoder(const byte which);
  float get_rpm();
};

#endif