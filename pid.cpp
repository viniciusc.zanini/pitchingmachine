#include "pid.h"

// constructor
PID::PID(float setP, float setI, float setD) {
  this->set_params(setP, setI, setD);
}

float PID::control(float read, float setPoint) {
  error = setPoint - read; // Proportional
  i += error;              // Integral
  d = error - lastError;   // Derivative

  lastError = error;

  if (error == 0)
    i = 0; // Prevent integral to overload

  // Set controller gains and return
  return kp * error + ki * i + kd * d;
}

void PID::set_params(float setP, float setI, float setD) {
  kp = setP;
  ki = setI;
  kd = setD;

  i = 0;
  d = 0;

  lastError = 0;
  error     = 0;
}
