#ifndef PID_h
#define PID_h

#include <Arduino.h>

class PID {
private:
  float kp;
  float ki;
  float kd;

  float i;
  float d;

  float lastError;
  float error;

public:
  PID(float setP, float setI, float setD);

  float control(float read, float setPoint);

  void set_params(float setP, float setI, float setD);
};

#endif
